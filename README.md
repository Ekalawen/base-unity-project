Startup project to quickly setup a new project from scratch.
1) Create base folders hierarchy.
2) Create base GameManager-OtherManagers scripts structure.
3) Create according prefabs for base Managers.
4) Create a first scene with a base GameManager.
5) Import some tools custom scripts.

You can download the content of the Assets folder as a .zip directly in GitLab for simplicity.

Don't forget the .gitignore.

To import MyBox :
1) Open UPM: "Window/Package Manager"
2) Click "+" button at the top left
3) Select "Add package from git URL" and paste following URL: https://github.com/Deadcows/MyBox.git
