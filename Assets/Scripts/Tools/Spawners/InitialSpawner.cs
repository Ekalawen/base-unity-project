﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialSpawner : Spawner {

    public float delay = 0.0f;

    public override void Initialize(ISpawnerManager spawnerManager)
    {
        base.Initialize(spawnerManager);
        StartCoroutine(CSpawnIn());
    }

    private IEnumerator CSpawnIn() {
        yield return new WaitForSeconds(delay);
        Spawn();
    }
}
