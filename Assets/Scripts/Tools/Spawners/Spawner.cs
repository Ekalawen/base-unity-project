﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Spawner : MonoBehaviour {

    public List<SpawningQuantity> spawningQuantities;

    protected ISpawnerManager manager;

    public virtual void Initialize(ISpawnerManager manager) {
        this.manager = manager;
    }

    public List<MonoBehaviour> Spawn() {
        return manager.SpawnSpawningQuantity(spawningQuantities);
    }

    public virtual void Stop() { }

    public List<GameObject> GetPrefabs() {
        List<GameObject> prefabs = new List<GameObject>();
        foreach(SpawningQuantity sq in spawningQuantities) {
            prefabs.AddRange(sq.GetPrefabs());
        }
        return prefabs;
    }
}
