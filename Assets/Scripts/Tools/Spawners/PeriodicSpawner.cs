﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeriodicSpawner : Spawner {

    public float firstDelay = 0.0f;
    public float periode = 5.0f;

    protected Coroutine spawningCoroutine;

    public override void Initialize(ISpawnerManager spawnerManager) {
        base.Initialize(spawnerManager);
        spawningCoroutine = StartCoroutine(PeriodicSpawning());
    }

    protected IEnumerator PeriodicSpawning() {
        yield return new WaitForSeconds(firstDelay);
        while(true) {
            yield return new WaitForSeconds(periode);
            Spawn();
        }
    }

    public override void Stop() {
        base.Stop();
        if(spawningCoroutine != null) {
            StopCoroutine(spawningCoroutine);
        } 
    }
}
