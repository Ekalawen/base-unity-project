﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISpawnerManager {

    public List<MonoBehaviour> SpawnSpawningQuantity(List<SpawningQuantity> spawningQuantities);

    public Spawner AddSpawner(Spawner spawner);

    public bool RemoveSpawner(Spawner spawner);
}
