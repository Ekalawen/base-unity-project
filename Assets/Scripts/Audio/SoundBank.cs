using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Custom/SoundBank", fileName = "SoundBank")]
public class SoundBank : ScriptableObject {

    public AudioClipParams firstSound;

}
