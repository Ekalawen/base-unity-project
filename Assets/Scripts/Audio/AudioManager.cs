using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class AudioManager : FastMonoBehaviour
{
    public static float DEFAULT_MUSIC_VOLUME = 0.5f;
    public static float DEFAULT_SOUND_VOLUME = 0.5f;

    public SoundBank sounds;
    public List<AudioClipParams> levelMusics;

    protected Transform globalSoundsFolder;
    protected List<AudioSource> availableSources;
    protected List<AudioSource> usedSources;
    protected List<AudioSource> pausedSources;
    protected AudioSource levelMusicSource;
    protected AudioClipParams levelMusic;
    protected float musicVolume;
    protected float soundVolume;

    public void Initialize()
    {
        InitializeAudioVolumes();
        globalSoundsFolder = new GameObject("Sounds").transform;
        availableSources = new List<AudioSource>();
        usedSources = new List<AudioSource>();
        pausedSources = new List<AudioSource>();
        StartLevelMusic();
    }

    public void InitializeAudioVolumes()
    {
        musicVolume = DEFAULT_MUSIC_VOLUME;
        soundVolume = DEFAULT_SOUND_VOLUME;
    }

    public void ApplyAudioVolumes() {
        InitializeAudioVolumes();
        foreach (AudioSource source in usedSources) {
            SetAudioSourceVolumeBasedOnRelativeVolume(source);
        }
        SetAudioSourceVolumeBasedOnRelativeVolume(levelMusicSource);
    }

    protected void SetAudioSourceVolumeBasedOnRelativeVolume(AudioSource audioSource)
    {
        AudioClipParamsHolder holder = audioSource.GetComponent<AudioClipParamsHolder>();
        float relativeVolume = holder.clipParams.relativeVolume;
        float typeMusicVolume = holder.clipParams.bIsMusic ? musicVolume : soundVolume;
        audioSource.volume = relativeVolume * typeMusicVolume * AudioClipParams.BASE_VOLUME;
    }

    protected void SetAudioSourceVolumeBasedOnNewRelativeVolume(AudioSource audioSource, float newRelativeVolume)
    {
        AudioClipParamsHolder holder = audioSource.GetComponent<AudioClipParamsHolder>();
        holder.clipParams.relativeVolume = newRelativeVolume;
        SetAudioSourceVolumeBasedOnRelativeVolume(audioSource);
    }

    public AudioSource GetAvailableSource()
    {
        // On v�rifie si celles d�j� utilis�es ont finit d'�tre utilis�es
        for (int i = 0; i < usedSources.Count; i++)
        {
            AudioSource source = usedSources[i];
            if (source == null)
            {
                usedSources.RemoveAt(i);
                i--;
                continue;
            }
            if (source.isPlaying == false)
            {
                availableSources.Add(source);
                usedSources.RemoveAt(i);
                i--;
                continue;
            }
        }

        // Si on en a d�j� de disponible, c'est cool ! :D
        if (availableSources.Count > 0)
        {
            AudioSource res = availableSources[0];
            availableSources.RemoveAt(0);
            usedSources.Add(res);
            return res;
        }

        // Sinon on va devoir en cr�er une ! :)
        AudioSource newSource = new GameObject("Source").AddComponent<AudioSource>();
        AudioClipParamsHolder clipHolder = newSource.gameObject.AddComponent<AudioClipParamsHolder>();
        usedSources.Add(newSource);
        return newSource;
    }

    protected AudioSource PlayClipsOnSource(
        AudioClipParams audioClipParams,
        Vector3 pos = new Vector3(),
        Transform parent = null,
        float duration = -1.0f,
        float acceleration = 1.0f,
        AudioSource sourceToUse = null,
        int clipIndice = -1,
        float avancementTime = -1)
    {

        // On get la source
        AudioSource source = sourceToUse ?? GetAvailableSource();
        if (source == null)
        {
            Debug.Log($"SOURCE NUL OMG !!! XD");
        }

        // On set le volume
        if (audioClipParams.bIsMusic)
        {
            source.volume = musicVolume;
        }
        else
        {
            source.volume = soundVolume;
        }
        source.volume *= AudioClipParams.BASE_VOLUME * audioClipParams.relativeVolume;

        // On positionne la source
        source.transform.position = pos;
        source.transform.SetParent((parent == null) ? globalSoundsFolder : parent);

        // On get le bon clip
        AudioClip clip = clipIndice == -1 ? MathTools.ChoseOne(audioClipParams.clips) : audioClipParams.clips[clipIndice];
        source.clip = clip;
        source.GetComponent<AudioClipParamsHolder>().clipParams = new AudioClipParams(audioClipParams); // Copy because we don't want to destroy the SoundBankIngame.asset ! :)

        // On v�rifie si c'est reverse ou pas
        if (audioClipParams.bReverse)
        {
            source.timeSamples = source.clip.samples - 1;
            source.pitch = -1;
        }
        else
        {
            source.timeSamples = 0;
            source.pitch = 1;
        }

        if (avancementTime != -1)
        {
            source.time = avancementTime;
        }

        // On ajuste le pitch pour matcher avec la duration si celle-ci est sp�cifi�e ! :)
        if (duration == -1)
        {
            source.pitch = source.pitch / Mathf.Abs(source.pitch); // Normalize � 1
        }
        else
        {
            float scaledDuration = duration / Time.timeScale;
            float accel = clip.length / scaledDuration;
            source.pitch *= accel;
        }
        source.pitch *= acceleration;

        // On set le spatial blend !
        source.spatialBlend = audioClipParams.spatialBlend;

        // On v�rifie si �a loop ou pas !
        source.loop = audioClipParams.bLoop;

        // Et enfin on joue la source ! <3
        source.Play();

        return source;
    }

    protected IEnumerator StopClipIn(AudioSource source, float duree)
    {
        yield return new WaitForSeconds(duree);
        source.Stop();
    }

    public void PauseSounds()
    {
        foreach (AudioSource source in usedSources)
        {
            source.Pause();
            pausedSources.Add(source);
        }
        usedSources.Clear();
    }

    public void UnPauseSounds()
    {
        foreach (AudioSource source in pausedSources)
        {
            source.UnPause();
            usedSources.Add(source);
        }
        pausedSources.Clear();
    }

    protected void StartLevelMusic() {
        if(levelMusics.Count == 0) {
            Debug.LogWarning("Il n'y a pas de levelMusics dans le SoundManager � lancer ! :)");
            return;
        }
        Assert.AreNotEqual(levelMusics.Count, 0);
        levelMusic = MathTools.ChoseOne(levelMusics);
        levelMusicSource = PlayClipsOnSource(levelMusic);
        usedSources.Remove(levelMusicSource);
    }

    protected void FadeOutSourceIn(AudioSource source, float duration, float waitDuration = 0)
    {
        StartCoroutine(CFadeOutSource(source, duration, waitDuration));
    }

    protected IEnumerator CFadeOutSource(AudioSource source, float duration, float waitDuration)
    {
        yield return new WaitForSeconds(waitDuration);
        float maxVolume = source.volume;
        Timer timer = new Timer(duration);
        while (!timer.IsOver())
        {
            float newVolume = maxVolume * (1 - timer.GetAvancement());
            source.volume = newVolume;
            yield return null;
        }
        source.volume = 0.0f;
        source.Stop();
        /// Why would we want musics to go back in the pool? (I only fadeInFadeOut musics, never SFXs x)) ==> This caused a bug when finishedLevelMusic goes back into the pool and made some SFX have 0 volume because of UpdateIREndGameVolume :)
        //if (!availableSources.Contains(source)) {
        //    availableSources.Add(source);
        //}
    }

    protected void FadeInSourceIn(AudioSource source, float duration, float waitDuration = 0)
    {
        StartCoroutine(CFadeInSource(source, duration, waitDuration));
    }

    protected IEnumerator CFadeInSource(AudioSource source, float duration, float waitDuration)
    {
        yield return new WaitForSeconds(waitDuration);
        float maxVolume = source.volume;
        Timer timer = new Timer(duration);
        while (!timer.IsOver())
        {
            float newVolume = maxVolume * timer.GetAvancement();
            source.volume = newVolume;
            yield return null;
        }
        source.volume = maxVolume;
    }

    // ------------------------------------- //
    // Play clips functions start here ! :) //
    // ------------------------------------- //

    public void PlayFirstSound(Vector3 pos) {
        PlayClipsOnSource(sounds.firstSound, pos);
    }
}
